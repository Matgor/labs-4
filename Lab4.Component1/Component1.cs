﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Contract;
using ComponentFramework;

namespace Lab4.Component1
{
    public class Component1: AbstractComponent
    {
         IObliczenia obliczenia;

         public override void InjectInterface(Type type, object impl)
         {
              if (type == typeof(IObliczenia))
            {
                obliczenia = (IObliczenia)impl;
            }

             //throw new NotImplementedException();
         }

         public Component1() 
         {
             this.RegisterRequiredInterface(typeof(IObliczenia));
         }
       public Component1(IObliczenia obliczenia)
       {
           this.obliczenia = obliczenia;
       }
       

       public int Oblicz(int a, char znak, int b)
       {
           switch (znak)
           {
               case '+':
                   return a + b; //obliczenia.Dodaj(a, b);
               default:
                   return a - b; //obliczenia.Odejmij(a, b);
           }

       }

    }
}
