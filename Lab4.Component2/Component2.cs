﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Contract;
using ComponentFramework;

namespace Lab4.Component2
{
    public class Component2 : AbstractComponent, IObliczenia
    {
        public Component2()
        {
            this.RegisterProvidedInterface(typeof(IObliczenia), this);
        }

        public int Dodaj(int a, int b)
        {
            return a + b;
        }

        public int Odejmij(int a, int b)
        {
            return a - b;
        }

        public int Oblicz(int a,char znak, int b)
        {
            throw new NotImplementedException();
        }

        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }
    }
}
