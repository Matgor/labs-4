﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component2B
{
    public class Component2B : AbstractComponent, IObliczenia 
    {
        public override void InjectInterface(Type type, object impl)
        {
            Console.ReadKey();
        }

        public Component2B()
        {
            this.RegisterProvidedInterface(typeof(IObliczenia), this);

        }

        public int Dodaj(int a, int b)
        {
            return a + a;
        }

        public int Oblicz(int a, char znak, int b)
        {
            throw new NotImplementedException();
        }

        public int Odejmij(int a, int b)
        {
            return b - a;
        }
    }
}
