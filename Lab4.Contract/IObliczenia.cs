﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Contract
{
    public interface IObliczenia
    {
        int Dodaj(int a, int b);
        int Odejmij(int a, int b);
        int Oblicz(int a, char znak, int b);
    }
}
