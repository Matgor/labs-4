﻿using System;
using ComponentFramework;
using Lab4.Component1;
using Lab4.Component2;
using Lab4.Contract;
using Lab4.Component2B;

namespace Lab4.Main
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component2);
        public delegate void RegisterComponentInContainer(object container, object component);
        public delegate bool AreDependenciesResolved(object container);

        #endregion

        #region P1

        public static Type Component1 = typeof(Lab4.Component1.Component1);
        public static Type Component2 = typeof(Lab4.Component2.Component2);

        public static Type RequiredInterface = typeof(Lab4.Contract.IObliczenia);

        public static GetInstance GetInstanceOfRequiredInterface = (component2) => component2;
        
        #endregion

        #region P2

        public static Type Container = typeof(ComponentFramework.Container);

        public static RegisterComponentInContainer RegisterComponent = (container, component) => {
                ((IContainer)container).RegisterComponents((IComponent)component);
        };

        public static AreDependenciesResolved ResolvedDependencied = (container) => 
            (container as Container).DependenciesResolved;

        #endregion

        #region P3

        public static Type Component2B = typeof(Component2B.Component2B);

        #endregion
    }
}
