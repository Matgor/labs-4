﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Component1;
using Lab4.Component2;
using Lab4.Component2B;
using Lab4.Contract;
namespace Lab4.Main
{
    public class Program
    {
        static void Main(string[] args)
        {
            int a = 20;
            int b = 15;

            Component1.Component1 compo = new Component1.Component1();
            Console.WriteLine(compo.Oblicz(a, '+', b));
            Component2.Component2 compo2 = new Component2.Component2();
            Console.WriteLine(compo2.Dodaj(a, b));
            Console.WriteLine(compo2.Odejmij(a, b));
            Console.ReadKey();
        }
    }
}
